﻿import java.util.Scanner;


class Fahrkartenautomat
{
	static Scanner tastatur = new Scanner(System.in);
	
	public static void main(String[] args)
    {
	
	while(true) {
       double zuZahlenderBetrag = fahrkartenbestellungErfassen(); // Bestellung
       double rueckgabewert = fahrkartenBezahlen(zuZahlenderBetrag); // Geldeinwurf
       fahrkartenAusgeben(250); // Fahrscheinausgabe
       rueckgeldAusgeben(rueckgabewert); //Ausgabeberechnung
       System.out.print("\n\n\n");
	}
    }
	
    //Bestellung
   public static double fahrkartenbestellungErfassen() {
    	double zuZahlenderBetrag;
    	byte anzahl;
    	
    	System.out.print("Zu zahlender Betrag (EURO): ");
        zuZahlenderBetrag = tastatur.nextDouble();
        
        System.out.print("Anzahl der Tickets: ");
        anzahl = tastatur.nextByte(); //die Anzahl wird eingeben, mit dem zuZahlendenBetrag multipliziert, sodass ein neuer Wert entsteht,der neue zuZahledeBetrag 
        
        zuZahlenderBetrag = anzahl * zuZahlenderBetrag; //Der Einzelpreis wird mit der genannten Anzahl multipliziert. So entsteht der Preis für die Menge an Tickets
        
 	
        return zuZahlenderBetrag;
    } 
    
    //Geldeinwurf
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	 	double eingezahlterGesamtbetrag = 0.0;
    	 	
    	 	while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
      	   
    	 			System.out.printf("Noch zu zahlen: %.2f EURO\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
    	 			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	 			double eingeworfeneMünze = tastatur.nextDouble();
    	 			eingezahlterGesamtbetrag += eingeworfeneMünze;
    	 			System.out.println(eingezahlterGesamtbetrag);
    	 	}
    	 	
    	 return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }
    
    //Fahrscheinausgabe
   public static void fahrkartenAusgeben(int timer) {
	   System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          
          try {
			Thread.sleep(timer);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");
       }
   
   //Rückgeld- und Ausgabeberechnung 
   public static void rueckgeldAusgeben(double rückgabebetrag) {
	   
       if(rückgabebetrag > 0.0)
       {
    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f", rückgabebetrag).print(" Euro \n");
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
      
    }
   
  }
